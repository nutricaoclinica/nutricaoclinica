package Ferramentas;

import DAO.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

public class JTableF {

    public static void preencher(String table_name, String statement, javax.swing.JTable jTable) {
        //Obter nome das colunas da tabela
        ArrayList<String> column_name = Tabela.obterNomeColunas(table_name);

        //Definir as colunas da tabela
        jTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{null}, column_name.toArray()
        ));

        //Resetar a tabela
        jTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        DefaultTableModel dtm = (DefaultTableModel) jTable.getModel();
        dtm.setRowCount(1);

        //Preencher a tabela
        dtm = (DefaultTableModel) jTable.getModel();
        ArrayList<ArrayList> rows = Tabela.listar(statement);

        try {
            for (int rowIndex = 0; rowIndex < rows.size(); rowIndex++) {
                dtm.addRow(rows.get(rowIndex).toArray());
            }
        } catch (NullPointerException ex) {

        }
    }

    public static void inserirModificacoes(String table_name, javax.swing.JTable jTable) {
        for (int rowIndex = 0; rowIndex < jTable.getRowCount(); rowIndex++) {
            ArrayList<Object> row = new ArrayList<>();
            for (int columnIndex = 0; columnIndex < jTable.getColumnCount(); columnIndex++) {
                //Obter valor da tabela na posicao (rowIndex, columnIndex)
                try {
                    row.add(Double.parseDouble(String.valueOf(jTable.getValueAt(rowIndex, columnIndex))));
                } catch (NumberFormatException ex) {
                    row.add(jTable.getValueAt(rowIndex, columnIndex));
                }

                //Substituir valores vazios por 0
                if (columnIndex > 0 && row.get(0) == null) {
                    row.set(columnIndex, 0);
                }
            }

            //Inserir ou atualizar elemento
            if (rowIndex == 0 && row.get(0) != null && !row.get(0).equals("")) {
                Registro.inserir(table_name, row);
            } else {
                Registro.atualizar(table_name, row);
            }
        }
    }

    public static void jPopupMenu(String table_name, java.awt.event.MouseEvent evt, javax.swing.JTable jTable) {
        if (SwingUtilities.isRightMouseButton(evt)) {
            javax.swing.JPopupMenu jPopupMenu = new javax.swing.JPopupMenu();
            javax.swing.JMenu jMenu = new javax.swing.JMenu("Deletar");
            Object selecionado = jTable.getValueAt(jTable.rowAtPoint(evt.getPoint()), 0);
            jMenu.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt1) {
                    Registro.remover(table_name, selecionado);
                    preencher(table_name, Variaveis.listarTudo(table_name), jTable);
                    jPopupMenu.setVisible(false);
                    //if(table_name.equals("tipo_de_alimentacao")){
                    //    ListaDeAlimentosPermitidosDAO.removerTipoDeAlimentacao(Integer.parseInt(String.valueOf(selecionado)));
                    //}
                }
            });
            jPopupMenu.add(jMenu);
            jPopupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }
}
