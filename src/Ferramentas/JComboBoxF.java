
package Ferramentas;

import DAO.Tabela;
import javax.swing.DefaultComboBoxModel;

public class JComboBoxF {
    public static void preencherNomeTabelas(javax.swing.JComboBox jComboBox) {
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
        for (int i = 0; i < Tabela.obterNomeTabelas().size(); i++) {
            dcbm.addElement(Tabela.obterNomeTabelas().get(i));
        }
        jComboBox.setModel(dcbm);
    }
    
    public static void preencherNomeColunas(String table_name, javax.swing.JComboBox jComboBox1) {
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
        for (int i = 0; i < Tabela.obterNomeColunas(table_name).size(); i++) {
            dcbm.addElement(Tabela.obterNomeColunas(table_name).get(i));
        }
        jComboBox1.setModel(dcbm);
    }
}
