
package Ferramentas.LDAP;

import javax.swing.SwingUtilities;

public class JTableF {
    public static void jPopupMenuDeletar(int codigo_tipo_de_alimentacao, java.awt.event.MouseEvent evt, javax.swing.JTable jTable1,  javax.swing.JTable jTable2) {
        if (SwingUtilities.isRightMouseButton(evt)) {
            javax.swing.JPopupMenu jPopupMenu = new javax.swing.JPopupMenu();
            javax.swing.JMenu jMenu1 = new javax.swing.JMenu("Deletar");
            Object selecionado = jTable1.getValueAt(jTable1.rowAtPoint(evt.getPoint()), 0);
            jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt1) {
                    DAO.LDAP.AlimentoPermitido.remover(Integer.parseInt(String.valueOf(selecionado)), codigo_tipo_de_alimentacao);
                    Ferramentas.JTableF.preencher("alimentos", DAO.LDAP.Variaveis.LISTAR_ALIMENTOS_PERMITIDOS + codigo_tipo_de_alimentacao, jTable1);
                    Ferramentas.JTableF.preencher("alimentos", DAO.LDAP.Variaveis.LISTAR_ALIMENTOS_PROIBIDOS + codigo_tipo_de_alimentacao + ")", jTable2);
                    jPopupMenu.setVisible(false);
                }
            });
            jPopupMenu.add(jMenu1);
            jPopupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }
    
    public static void jPopupMenuAdicionar(int codigo_tipo_de_alimentacao, java.awt.event.MouseEvent evt, javax.swing.JTable jTable2, javax.swing.JTable jTable1) {
        if (SwingUtilities.isRightMouseButton(evt)) {
            javax.swing.JPopupMenu jPopupMenu = new javax.swing.JPopupMenu();
            javax.swing.JMenu jMenu1 = new javax.swing.JMenu("Adicionar");
            Object selecionado = jTable2.getValueAt(jTable2.rowAtPoint(evt.getPoint()), 0);
            jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt1) {
                    DAO.LDAP.AlimentoPermitido.adicionar(Integer.parseInt(String.valueOf(selecionado)), codigo_tipo_de_alimentacao);
                    Ferramentas.JTableF.preencher("alimentos", DAO.LDAP.Variaveis.LISTAR_ALIMENTOS_PROIBIDOS + codigo_tipo_de_alimentacao + ")", jTable2);
                    Ferramentas.JTableF.preencher("alimentos", DAO.LDAP.Variaveis.LISTAR_ALIMENTOS_PERMITIDOS + codigo_tipo_de_alimentacao, jTable1);
                    jPopupMenu.setVisible(false);
                }
            });
            jPopupMenu.add(jMenu1);
            jPopupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }
}
