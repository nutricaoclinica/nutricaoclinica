
package Ferramentas.LDAP;

import javax.swing.DefaultComboBoxModel;

public class JComboBoxF {
    public static void preencherNomeTipoDeAlimentacao(javax.swing.JComboBox jComboBox1) {
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
        for (int i = DAO.LDAP.TipoDeAlimentacao.obterTipoDeAlimentacao().size() - 1; i >= 0; i--) {
            dcbm.addElement(DAO.LDAP.TipoDeAlimentacao.obterTipoDeAlimentacao().get(i));
        }
        jComboBox1.setModel(dcbm);
    }
}
