package DAO.LDAP;

import DAO.Conexao;

import java.util.logging.*;
import java.util.*;
import java.sql.*;

public class TipoDeAlimentacao {

    public static ArrayList<String> obterTipoDeAlimentacao() {
        try {
            //Obter nome das colunas da tabela
            String statement = "SELECT nome FROM nutricao_clinica_sc.tipo_de_alimentacao";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);

            ResultSet rs = ps.executeQuery();

            ArrayList<String> table_name = new ArrayList();

            while (rs.next()) {
                table_name.add(rs.getString("nome"));
            }
            c.close();
            //Retornar nome das colunas da tabela
            return table_name;
        } catch (SQLException ex) {
            Logger.getLogger(TipoDeAlimentacao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void inserirTipoDeAlimentacao(int codigo_tipo_de_alimentacao) {
        try {
            //Obter código de todos os alimentos
            String statement = "SELECT codigo FROM nutricao_clinica_sc.alimentos";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);

            ResultSet rs = ps.executeQuery();

            ArrayList<Integer> codigo_alimento = new ArrayList<>();
            while (rs.next()) {
                codigo_alimento.add(rs.getInt("codigo"));
            }

            //Inserir os códigos dos alimentos nesse codigo do tipo de alimentação
            statement = "INSERT INTO nutricao_clinica_sc.lista_de_alimentos_permitidos (codigo_do_tipo_de_alimentacao, codigo_do_alimento) VALUES (?, ?)";
            for (int i = 0; i < codigo_alimento.size(); i++) {
                ps = Conexao.obterConexao().prepareStatement(statement);
                ps.setInt(1, codigo_tipo_de_alimentacao);
                ps.setInt(2, codigo_alimento.get(i));
                ps.execute();
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoDeAlimentacao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void removerTipoDeAlimentacao(int codigo_tipo_de_alimentacao) {
        try {
            //Gerar e executar SQL de remoção
            String statement = "DELETE FROM nutricao_clinica_sc.lista_de_alimentos_permitidos WHERE codigo_do_tipo_de_alimentacao = ?";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);

            ps.setObject(1, codigo_tipo_de_alimentacao);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoDeAlimentacao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
