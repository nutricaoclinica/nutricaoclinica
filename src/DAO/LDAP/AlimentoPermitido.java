package DAO.LDAP;

import DAO.Conexao;

import java.util.logging.*;
import java.sql.*;

public class AlimentoPermitido {

    public static void remover(int codigo_alimento, int codigo_tipo_de_alimentacao) {
        try {
            //Gerar e executar SQL de remoção
            String statement = "DELETE FROM nutricao_clinica_sc.lista_de_alimentos_permitidos WHERE codigo_do_alimento = ? and codigo_do_tipo_de_alimentacao = ?";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            
            ps.setObject(1, codigo_alimento);
            ps.setObject(2, codigo_tipo_de_alimentacao);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlimentoPermitido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void adicionar(int codigo_alimento, int codigo_tipo_de_alimentacao) {
        try {
            //Gerar e executar SQL de remoção
            String statement = "INSERT INTO nutricao_clinica_sc.lista_de_alimentos_permitidos(codigo_do_tipo_de_alimentacao, codigo_do_alimento) VALUES (?,  ?);";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            
            ps.setObject(1, codigo_tipo_de_alimentacao);
            ps.setObject(2, codigo_alimento);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(AlimentoPermitido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
