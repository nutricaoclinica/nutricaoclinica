package DAO.LDAP;

public class Variaveis {
    public static String LISTAR_ALIMENTOS_PERMITIDOS =  "SELECT nutricao_clinica_sc.alimentos.*\n"
            + "FROM nutricao_clinica_sc.alimentos JOIN nutricao_clinica_sc.lista_de_alimentos_permitidos\n"
            + "ON nutricao_clinica_sc.alimentos.codigo = nutricao_clinica_sc.lista_de_alimentos_permitidos.codigo_do_alimento\n"
            + "JOIN nutricao_clinica_sc.tipo_de_alimentacao\n"
            + "ON nutricao_clinica_sc.lista_de_alimentos_permitidos.codigo_do_tipo_de_alimentacao = nutricao_clinica_sc.tipo_de_alimentacao.codigo\n"
            + "WHERE nutricao_clinica_sc.tipo_de_alimentacao.codigo = ";
    
    public static String LISTAR_ALIMENTOS_PROIBIDOS = "SELECT nutricao_clinica_sc.alimentos.*\n"
            + "FROM nutricao_clinica_sc.alimentos\n"
            + "WHERE nutricao_clinica_sc.alimentos.codigo\n"
            + "NOT IN (SELECT nutricao_clinica_sc.alimentos.codigo\n"
            + "FROM nutricao_clinica_sc.alimentos\n"
            + "JOIN nutricao_clinica_sc.lista_de_alimentos_permitidos\n"
            + "ON nutricao_clinica_sc.alimentos.codigo = nutricao_clinica_sc.lista_de_alimentos_permitidos.codigo_do_alimento\n"
            + "JOIN nutricao_clinica_sc.tipo_de_alimentacao\n"
            + "ON nutricao_clinica_sc.lista_de_alimentos_permitidos.codigo_do_tipo_de_alimentacao = nutricao_clinica_sc.tipo_de_alimentacao.codigo\n"
            + "WHERE nutricao_clinica_sc.tipo_de_alimentacao.codigo = ";
}
