package DAO;

import java.util.logging.*;
import java.util.*;
import java.sql.*;

public class Registro {

    public static void inserir(String table_name, ArrayList<Object> values) {
        try {
            //Obter nome das colunas da tabela
            ArrayList<String> column_name = Tabela.obterNomeColunas(table_name);
            
            //Gerar e executar SQL de inserção
            String statement = "INSERT INTO " + Variaveis.SCHEMA + "." + table_name + "(";
            for (int i = 0; i < column_name.size(); i++) {
                statement += column_name.get(i);
                if (i < column_name.size() - 1) {
                    statement += ", ";
                }
            }
            statement += ") VALUES (";
            for (int i = 0; i < column_name.size(); i++) {
                statement += "?";
                if (i < column_name.size() - 1) {
                    statement += ", ";
                }
            }
            statement += ")";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            
            for (int i = 0; i < values.size(); i++) {
                ps.setObject((i + 1), values.get(i));
            }
            ps.executeUpdate();
            c.close();
            
            if(table_name.equals("tipo_de_alimentacao")){
                DAO.LDAP.TipoDeAlimentacao.inserirTipoDeAlimentacao(Integer.parseInt(values.get(0).toString().substring(0,1)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void atualizar(String table_name, ArrayList<Object> values) {
        try {
            //Obter nome das colunas da tabela
            ArrayList<String> column_name = Tabela.obterNomeColunas(table_name);

            //Gerar e executar SQL de atualização
            String statement = "UPDATE " + Variaveis.SCHEMA + "." + table_name + " SET ";
            for (int i = 0; i < column_name.size(); i++) {
                statement += column_name.get(i) + " = ?";
                if (i < column_name.size() - 1) {
                    statement += ", ";
                }
            }
            statement += " WHERE " + column_name.get(0) + " = ?";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            
            for (int i = 0; i < values.size(); i++) {
                ps.setObject((i + 1), values.get(i));
            }
            ps.setObject(values.size() + 1, values.get(0));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void remover(String table_name, Object primary_key_value) {
        try {
            //Obter nome da chave primária da tabela
            String table_primary_key = Tabela.obterNomeColunas(table_name).get(0);

            //Gerar e executar SQL de remoção
            String statement = "DELETE FROM " + Variaveis.SCHEMA + "." + table_name + " WHERE " + table_primary_key + " = ?";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            
            ps.setObject(1, primary_key_value);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
