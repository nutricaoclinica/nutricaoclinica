package DAO;

import java.util.logging.*;
import java.util.*;
import java.sql.*;

public class Tabela {

    public static ArrayList<String> obterNomeTabelas() {
        try {
            String statement = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ?";
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);

            ps.setString(1, Variaveis.SCHEMA);
            ResultSet rs = ps.executeQuery();

            ArrayList<String> table_name = new ArrayList();

            while (rs.next()) {
                table_name.add(rs.getString("table_name"));
            }

            c.close();
            return table_name;
        } catch (SQLException ex) {
            Logger.getLogger(Tabela.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ArrayList<String> obterNomeColunas(String table_name) {
        try {
            String statement = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";

            //Provisório enquanto o banco não está atualizado/migrado
            statement += " and TABLE_SCHEMA = '" + Variaveis.SCHEMA + "'";

            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);

            ps.setString(1, table_name);
            ResultSet rs = ps.executeQuery();

            ArrayList<String> column_name = new ArrayList();
            while (rs.next()) {
                column_name.add(rs.getString("COLUMN_NAME"));
            }
            c.close();
            return column_name;
        } catch (SQLException ex) {
            Logger.getLogger(Tabela.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ArrayList<ArrayList> listar(String statement) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement(statement);
            ResultSet rs = ps.executeQuery();
            ArrayList<ArrayList> rows = new ArrayList<>();

            while (rs.next()) {
                ArrayList<Object> columns = new ArrayList<>();
                for (int columnIndex = 1; columnIndex <= rs.getMetaData().getColumnCount(); columnIndex++) {
                    columns.add(rs.getObject(rs.getMetaData().getColumnName(columnIndex)));
                    if (columnIndex == rs.getMetaData().getColumnCount()) {
                        rows.add(columns);
                    }
                }
            }
            c.close();
            return rows;
        } catch (SQLException ex) {
            //Logger.getLogger(Tabela.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
