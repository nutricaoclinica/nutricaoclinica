
package DAO;

import java.sql.*;
import java.util.logging.*;

public class Conexao {
    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://127.0.0.1/nutricao_clinica";
            String username = "admin";
            String password = "admin";
            return DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
